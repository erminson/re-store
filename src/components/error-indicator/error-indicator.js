import React from 'react';

import './error-indicator.css';

const ErrorIndicator = () => {
  return (
    <div>
      <h2>ERROR!</h2>
    </div>
  );
};

export default ErrorIndicator;
