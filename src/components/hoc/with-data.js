import React, { Component } from 'react';

import Spinner from '../spinner';
import ErrorIndicator from '../error-indicator';

const withData = (Wrappered) => {
  return class extends Component {
    state = {
      data: null,
      loading: true,
      error: false,
    };

    update = () => {
      this.setState({
        loading: true,
        error: false,
      });

      this.props
        .getDate()
        .then((data) => {
          this.setState({
            data,
            loading: false,
          });
        })
        .error(() => {
          this.setState({
            loading: false,
            error: true,
          });
        });
    };

    render() {
      const { data, loading, error } = this.props;
      if (loading) {
        return <Spinner />;
      }

      if (error) {
        return <ErrorIndicator />;
      }

      return <Wrappered {...this.props} data={data} />;
    }
  };
};

export default withData;
